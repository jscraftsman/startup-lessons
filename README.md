# Startup Lessons

Learning repository. 

This project contains lessons learned from experts in Startup industry **(summary and link references below)**.


## **Categories**
- [Starting a business](https://www.startups.com/library/expert-advice?page=32&filter=starting-a-business) - 1/33 pages
  - [Page 1 out of 33](https://www.startups.com/library/expert-advice?page=33&filter=starting-a-business) - 1/1 topics - [link to list of topics](#page-1-out-of-33)
  - [Page 2 out of 33](https://www.startups.com/library/expert-advice?page=32&filter=starting-a-business) - 5/9 topics - [link to list of topics](#page-2-out-of-33)

- [Growing a business](https://www.startups.com/library/expert-advice?page=24&filter=growing-a-business) - 0/24 pages
- [Running a business](https://www.startups.com/library/expert-advice?page=21&filter=running-a-business) - 0/21 pages

### **Starting a business**
List of topics (and corresponding links) read.

#### Page 1 out of 33
- [How To Get Angel Investor Agreements Using Win-Win Deal Structures](https://www.startups.com/library/expert-advice/angel-investment-deal-structures) - [Link to notes](#how-to-get-angel-investor-agreements-using-win-win-deal-structures)

#### Page 2 out of 33
- [Interview with John Tabis, Co-Founder of The Bouqs](https://www.startups.com/library/expert-advice/interview-john-tabis-the-bouqs) - [Link to notes](#interview-with-john-tabis-co-founder-of-the-bouqs)
- [Dynotag: Send Lost Luggage the Way of the Dinosaur](https://www.startups.com/library/expert-advice/send-lost-luggage-the-way-of-the-dinosaur) - [Link to notes](#dynotag-send-lost-luggage-the-way-of-the-dinosaur)
- [Crate Stuff &#8211; How Creation Crate is Fixing Tech-Ed](https://www.startups.com/library/expert-advice/crate-stuff-how-creation-crate-is-fixing-tech-ed) - [Link to notes](#crate-stuff-how-creation-crate-is-fixing-tech-ed)
- [How To Get Ahead With A Strategic Startup Partnership](https://www.startups.com/library/expert-advice/use-strategic-partnerships-to-help-your-startup-hit-the-big-time) - [Link to notes](#how-to-get-ahead-with-a-strategic-startup-partnership)
- [Slidebean: Professional Slide Presentation in Minutes](https://www.startups.com/library/expert-advice/slidebean-professional-slide-presentation-in-minutes) -  [Link to notes](#slidebean-professional-slide-presentation-in-minutes)

### **Growing a business**
List of topics (and corresponding links) read.

### **Running a business**
List of topics (and corresponding links) read.

## **Notes**

### **Starting a business**

#### How To Get Angel Investor Agreements Using Win-Win Deal Structures

<details>
<summary><em>Click me to collapse/fold.</em></summary>

Angel investment deal structures are all about creating a win-win situation. Determining the provisions is a critical juncture in every startup’s evolution.

How to get angel investor Agreements with win-win deal structures
1. Be Cautiously Optimistic
    - Make sure you thoroughly review any term sheet with a lawyer to make sure you completely understand the deal structure and terms.
2. The Two Most Common Seed Stage Angel Investments
    - The **equity stake** *(which isn’t really an instrument, it’s just what we’re calling it)* is simply you making the determination now.
    - The **convertible note** *(ok, that’s a real term)* suggests that you’ll both agree that the valuation will be determined at some later date based on a few criteria.
3. Equity Stake
    - The amount of equity the investor receives will depend upon the valuation that you agreed up on with the investor. So if you valued the company at $1,000,000 and the investor put in $150,000 of cash, they would get 15% of your company.
4. Convertible Note
    - Sometimes the investor and the entrepreneur cannot agree on exactly what valuation the company has today. In that case, they may opt to issue a convertible note that basically lets both parties set the value of the company at a later date, usually when more outside money comes in and values the company then.
5. The Term Sheet
    - A term sheet is simply a non-binding document that outlines the terms and conditions in which an investment is to be made.
6. It Ain’t Done Till it’s Done
    - The most common mistake first-time entrepreneurs make is thinking that a done deal is a done deal. It’s not.
    - Do not get cozy when you see a term sheet. Realize that it only means there is serious interest. You can’t rest until the check is in the bank. And even then, the work has just begun!

[Categories](#categories)

</details>

#### Interview with John Tabis, Co-Founder of The Bouqs

<details>
<summary><em>Click me to collapse/fold.</em></summary>

What happens when a Disney brand director and a Venezuelan flower farmer team up? A multimillion dollar flower company that is shaking up the florist industry.

- John Tabis was a Director of Corporate Brand Development at Disney.
- Juan Pablo (JP) Montufar-Arroyo A native Ecuadorian who grew up on a rose farm.
- The Bouqs, an online flower delivery company
- Neither of them had very much money so they went to friends and family and pulled together $13,000 to get started.
- That was four years ago. Today, they’re working with nearly $20 million in VC money — and John definitely doesn’t miss Disney.
- So what does it take to go from $13k to $20 million? 
    - John told me that the very first thing you need is a **"a product or service that resonates."**
    - > “This space has been ready for serious change for quite some time, and we’ve been lucky enough to present a solution that both consumers and florists were craving,” John says. 
    - > “Of course we’ve had to put a lot of hard work into the technology, communications and execution, but first and foremost you have to have a solution that’s loved.”
- John says that, in addition to resonating with both consumers and florists, The Bouqs has something that always pulls in that essential third group of people: investors. 
    - It’s a totally new approach to a huge market.
    - > “AirBnB is reinventing an entire category. Uber the same,” John says. 
    - > “And so are we. Just selling something online is an extremely commoditized approach. 
    - > Selling something online while changing that category forever is much harder, and much more valuable in the market and to investors. 
    - > By zeroing in on how you change the world and clearly communicating that to investors, you’ll make it much more likely to attract capital to that solution.”
- The team at The Bouqs has done a great job utilizing technology and design. 
    - John told me that they started with existing tools like Skype and email — **no need to reinvent the wheel there** — **and then developed their own custom technology in order to streamline and automate their processes**. 
    - It’s a great example of how good technology can reduce the workload for a small, rapidly growing startup.
- One look at The Bouqs website had me personally wanting to buy every single bouquet they offered. John tells me that, too, was very deliberate.
    - > “When I was exploring the industry before we started, one of the things that struck me was how out of date all of the shopping experiences were from a user experience and design perspective,” John says. 
    - > “The entire industry appeared to be trapped in the late 90s. Coming from Disney, I know how important the brand experience is to building a successful business, so from day one we focused heavily on design and user experience.”
- It’s those three things — a truly novel approach to an existing market, slick tech, and great design — that have brought John, JP, and The Bouqs from an idea backed by $13k to an international company backed by $20 million.

[Categories](#categories)

</details>

#### Dynotag: Send Lost Luggage the Way of the Dinosaur

<details>
<summary><em>Click me to collapse/fold.</em></summary>

[Dynotag](https://dynotag.com/) provides indestructible data luggage tags for any item you think you might lose.

The information associated with each tag is stored on Dynotag’s cloud and each tag is linked back to its own private website. When someone finds the tag, they can use it to access the owner’s contact information.

:warning: **No useful information (or lessons) regarding startup business**

[Categories](#categories)

</details>

#### Crate Stuff &#8211; How Creation Crate is Fixing Tech-Ed

<details>
<summary><em>Click me to collapse/fold.</em></summary>

[Creation Crate](https://www.creationcrate.com/) is a fun way to learn about tech - a subject often ignored by mainstream education - for anyone aged 12 and over. Creation Crate kits are delivered monthly by post and grow in difficulty and complexity.

:warning: **No useful information (or lessons) regarding startup business**

[Categories](#categories)

</details>

#### How To Get Ahead With A Strategic Startup Partnership

<details>
<summary><em>Click me to collapse/fold.</em></summary>

Find out how you can get your show on the road with a strategic startup partnership.

- In the early years of your startup, you may feel like a one-person show. 
- You have infinite faith in your product or service, but how do you translate that commitment to investors and stakeholders? 
- How can you raise brand awareness before you have the funding necessary for marketing and PR?
- Despite these obstacles, you must find a way to gain traction in building a community or generating sales. Otherwise, you risk stalling or, worse, folding. 

##### Getting ahead with a strategic startup partnership
1. Instant Street Cred
    - Validation by large enterprises generates the credibility and exposure you need to gain momentum and sales. Most important, it helps attract investors.
2. Exposure to a Large Customer Base
    - Startup brand partnerships offer volume. 
    - When you have access to a partner’s customer base, it’s much easier to drum up interest and develop brand equity. 
    - “The biggest challenge for a new company with a breakthrough product is generating awareness on a small budget,”
3. Ready-Made Mentors
    - When you partner with brands already familiar with your industry, it saves you time and headaches. 
    - You can learn from your partners’ challenges and generally view them as mentors.
    - Insights from industry experts go a long way in fast-tracking your startup — and can mean the difference between success and failure. 
4. Potential for Permanent Partnerships
    - Sometimes, a startup-brand partnership initiates a long-term gig. 
    - Many large enterprises lend support as a test run for future acquisition. 
    - For you, it’s a win-win: You gain support and access to the fast track now, and even if relationships aren’t long-term, you’ve still made valuable connections that propelled your business forward.
    - Despite how it may seem at times, your business is not a one-person show. 
    - Support from strategic partnerships can come in many forms: marketing exposure, validation, product testing, cost-saving services, or just good old-fashioned feedback. 
    - Pursuing these partnerships will help drive your business to the next level — and set you up for long-term success.

[Categories](#categories)

</details>

#### Slidebean: Professional Slide Presentation in Minutes

<details>
<summary><em>Click me to collapse/fold.</em></summary>

With Slidebean, templates and other presentation tools essentially put design on autopilot.

Slidebean allows anyone to create a professional slide presentation with barely any effort on the design end, so that presenters can devote their energies to the information that matters most.


:warning: **No useful information (or lessons) regarding startup business**

[Categories](#categories)

</details>

#### TOPIC

<details>
<summary><em>Click me to collapse/fold.</em></summary>

CONTENT HERE

[Categories](#categories)

</details>

## **Growing a business**
No content yet.

## **Running a business**
No content yet.

